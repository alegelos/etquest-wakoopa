//
//  IMGSessionMock.swift
//  Etquest-WakoopaTests
//
//  Created by Vicky on 5/1/18.
//  Copyright © 2018 AleG. All rights reserved.
//

import ImgurSession
@testable import Etquest_Wakoopa


class IMGSessionMock: IMGSession  {
    override func authenticate(withRefreshToken refreshToken: String!) {
        NotificationCenter.default.post(name: .didAuthenticateToImgGur, object: self, userInfo: ["testing":true])
    }
    
    override func authenticate() {
        NotificationCenter.default.post(name: .didAuthenticateToImgGur, object: self, userInfo: ["testing":true])

    }
    
    override func get(_ URLString: String!, parameters: [AnyHashable : Any]!, success: ((URLSessionDataTask?, Any?) -> Void)!, failure: ((Error?) -> Void)!) -> URLSessionDataTask! {
        let filePath = Bundle(for: type(of: self)).path(forResource: "TopKittiesSampleData", ofType: "json")
        let data = try! Data(contentsOf: URL(fileURLWithPath: filePath!), options: .mappedIfSafe)
        let jsonResult = try! JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
        success(URLSessionDataTask(), jsonResult)
        return URLSessionDataTask()
    }
}
