//
//  LoginPresenterTests.swift
//  Etquest-WakoopaTests
//
//  Created by Vicky on 5/1/18.
//  Copyright © 2018 AleG. All rights reserved.
//

import XCTest
@testable import Etquest_Wakoopa

class LoginPresenterTests: XCTestCase {
    
    let imgSessionMock = IMGSessionMock()
    var loginViewMock: LoginViewMock!
    var loginPresenterUnderTest: LoginPresenter!
    
    override func setUp() {
        loginViewMock = LoginViewMock()
        loginPresenterUnderTest = LoginPresenter(withImgSession: imgSessionMock)
        loginPresenterUnderTest.attach(loginView: loginViewMock)
    }
    
    func testShouldLogin() {
        loginPresenterUnderTest.tryAuthenticateWithRefreshToken()
        
        XCTAssertTrue(loginViewMock.didCallToShowMainView, "Should do autologin")
    }
    
    func testAuthenticate(){
        loginPresenterUnderTest.authenticate()
    
        XCTAssertTrue(loginViewMock.didCallToShowMainView, "Should log in")
    }
}
