//
//  LoginViewMock.swift
//  Etquest-WakoopaTests
//
//  Created by Vicky on 5/1/18.
//  Copyright © 2018 AleG. All rights reserved.
//

import XCTest
@testable import Etquest_Wakoopa

class LoginViewMock: NSObject, LoginView {
    var didCallToShowMainView = false
    
    
    func showMainView() {
        didCallToShowMainView = true
    }
}
