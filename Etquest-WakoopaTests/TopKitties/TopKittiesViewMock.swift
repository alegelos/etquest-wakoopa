//
//  TopKittiesViewMock.swift
//  Etquest-WakoopaTests
//
//  Created by Vicky on 5/2/18.
//  Copyright © 2018 AleG. All rights reserved.
//

import XCTest
import ImgurSession
@testable import Etquest_Wakoopa

class TopKittiesViewMock: NSObject, TopKittiesView {
    var isLoadingIndicatorAnimating = true
    var allMediaData: Array<IMGModel> = []
    var lastPageStartLoading = -1
    var calledNoInternet = false
    
    func startLoading(page: Int) {
        lastPageStartLoading = page
    }
    
    func stopLoading() {
        isLoadingIndicatorAnimating = false
    }
    
    func setData(withValues values: Array<IMGModel>) {
        allMediaData.append(contentsOf: values)
    }
    
    func showLoginView() {
        //TODO on logout or caducated refresh token
    }
    
    func noInternet() {
        calledNoInternet = true
    }
    
    
}


