//
//  TopKittiesPresenterTest.swift
//  Etquest-WakoopaTests
//
//  Created by Vicky on 5/2/18.
//  Copyright © 2018 AleG. All rights reserved.
//

import XCTest
import PromiseKit
import ImgurSession
@testable import Etquest_Wakoopa

class TopKittiesPresenterTest: XCTestCase {
    
    let imgSessionMock = IMGSessionMock()
    var topKittiesViewMock: TopKittiesViewMock!
    var topKittiesPresenterUnderTest: TopKittiesPresenter!
    
    
    override func setUp() {
        topKittiesViewMock = TopKittiesViewMock()
        topKittiesPresenterUnderTest = TopKittiesPresenter(withImgSession: imgSessionMock)
        topKittiesPresenterUnderTest.attach(topKittiesView: topKittiesViewMock)
        AFNetworkReachabilityManager.shared().stopMonitoring()
    }
    
    func testGetMoteCollectionViewData() {
        _ = topKittiesPresenterUnderTest.getMoreCollectionViewData(forValue: "cats")
            .ensure {
                XCTAssertEqual(self.topKittiesViewMock.allMediaData.count, 3)
        }
    }
    
    func testStopLoadingIndicator() {
        _ = topKittiesPresenterUnderTest.getMoreCollectionViewData(forValue: "cats")
            .ensure {
                XCTAssertFalse(self.topKittiesViewMock.isLoadingIndicatorAnimating)
        }
    }
    
    func testLoadedPagesAsyncCalls() {
        _ = topKittiesPresenterUnderTest.getMoreCollectionViewData(forValue: "cats")
            .ensure {
                XCTAssertEqual(self.topKittiesViewMock.lastPageStartLoading, 0)
        }
        _ = topKittiesPresenterUnderTest.getMoreCollectionViewData(forValue: "cats")
            .ensure {
                XCTAssertEqual(self.topKittiesViewMock.lastPageStartLoading, 1)
        }
        _ = topKittiesPresenterUnderTest.getMoreCollectionViewData(forValue: "cats")
            .ensure {
                XCTAssertEqual(self.topKittiesViewMock.lastPageStartLoading, 2)
        }
        _ = topKittiesPresenterUnderTest.getMoreCollectionViewData(forValue: "cats")
            .ensure {
                XCTAssertEqual(self.topKittiesViewMock.lastPageStartLoading, 3)
        }
    }
    
    func testLoadedPagesSyncCalls() {
        _ = topKittiesPresenterUnderTest.getMoreCollectionViewData(forValue: "cats")
            .ensure {
                XCTAssertEqual(self.topKittiesViewMock.lastPageStartLoading, 0)
                _ = self.topKittiesPresenterUnderTest.getMoreCollectionViewData(forValue: "cats")
                    .ensure {
                        XCTAssertEqual(self.topKittiesViewMock.lastPageStartLoading, 1)
                        _ = self.topKittiesPresenterUnderTest.getMoreCollectionViewData(forValue: "cats")
                            .ensure {
                                XCTAssertEqual(self.topKittiesViewMock.lastPageStartLoading, 2)
                                _ = self.topKittiesPresenterUnderTest.getMoreCollectionViewData(forValue: "cats")
                                    .ensure {
                                        XCTAssertEqual(self.topKittiesViewMock.lastPageStartLoading, 3)
                                }
                        }
                }
        }
    }
    
    func testNoInternet() {
        NotificationCenter.default.post(name: .didChangeInternetStatus, object: self, userInfo: ["status":AFNetworkReachabilityStatus.notReachable])
        XCTAssertTrue(topKittiesViewMock.calledNoInternet)
    }
}
