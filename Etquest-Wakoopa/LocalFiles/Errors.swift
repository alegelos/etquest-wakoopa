//
//  Errors.swift
//  Etquest-Wakoopa
//
//  Created by Vicky on 4/27/18.
//  Copyright © 2018 AleG. All rights reserved.
//

import Foundation

enum NetworkError: Error {
    case failToParseResponse, failToGetImageFromData, noInternet
}

enum ImgGurError: Error{
    case failToGetBaseURL, failToDoRequest, failToAuthenticate, failToGetRefreshToken, notAuthenticated, failToGetImageUrl, failToGetImage
}
