//
//  LoginView.swift
//  Etquest-Wakoopa
//
//  Created by Vicky on 4/28/18.
//  Copyright © 2018 AleG. All rights reserved.
//

import Foundation

protocol LoginView:class {
    func showMainView()
}
