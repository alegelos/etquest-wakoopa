
//
//  LoginPresenter.swift
//  Etquest-Wakoopa
//
//  Created by Vicky on 4/28/18.
//  Copyright © 2018 AleG. All rights reserved.
//

import Foundation
import ImgurSession

final class LoginPresenter {
    private var imgGurSession:IMGSession
    weak private var loginView: LoginView?
    
    init(withImgSession imgGurSession:IMGSession = IMGSession.sharedInstance()) {
        self.imgGurSession = imgGurSession
        NotificationCenter.default.addObserver(self, selector: #selector(didLogin(noti:)), name: .didAuthenticateToImgGur, object: nil)
    }
    
    func attach(loginView: LoginView) {
        self.loginView = loginView
    }
    
    func detachView() {
        loginView = nil
    }
    
    func tryAuthenticateWithRefreshToken() {
        guard let refreshToken = UserUtils.getUserRefreshToken() else{return}
        
        imgGurSession.authenticate(withRefreshToken: refreshToken)
    }
    
    func authenticate(){
        imgGurSession.authenticate()
    }
    
    @objc private func didLogin(noti:Notification) {
        //Only unit testing will send userInfo else will be nil
        guard UIApplication.shared.keyWindow?.rootViewController?.presentedViewController == nil || noti.userInfo != nil else{return}
        loginView?.showMainView()
    }
    
}
