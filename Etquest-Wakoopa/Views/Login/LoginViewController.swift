//
//  LoginViewController.swift
//  Etquest-Wakoopa
//
//  Created by Vicky on 4/28/18.
//  Copyright © 2018 AleG. All rights reserved.
//

import UIKit
import ImgurSession

final class LoginViewController: UIViewController {
    
    private let loginPresenter = LoginPresenter(withImgSession: IMGSession.sharedInstance())
    private let kMainViewSegueIdentifier = "ShowMainView"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loginPresenter.attach(loginView: self)
        loginPresenter.tryAuthenticateWithRefreshToken()
    }
    
    @IBAction func didPressLoginWithImgGur(_ sender: Any) {
        loginPresenter.authenticate()
    }
}

//MARK: - LoginView Delegate
extension LoginViewController: LoginView{
    func showMainView() {
        performSegue(withIdentifier: kMainViewSegueIdentifier, sender: self)
    }
}
