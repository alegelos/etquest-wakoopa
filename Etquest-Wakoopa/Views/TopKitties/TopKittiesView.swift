//
//  TopKittiesView.swift
//  Etquest-Wakoopa
//
//  Created by Vicky on 4/29/18.
//  Copyright © 2018 AleG. All rights reserved.
//

import Foundation
import ImgurSession

protocol TopKittiesView:class {
    func startLoading(page:Int)
    func stopLoading()
    func setData(withValues values:Array<IMGModel>)
    func showLoginView()
    func noInternet()
}
