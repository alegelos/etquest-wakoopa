//
//  TopKittiesPresenter.swift
//  Etquest-Wakoopa
//
//  Created by Vicky on 4/29/18.
//  Copyright © 2018 AleG. All rights reserved.
//

import Foundation
import ImgurSession
import PromiseKit

final class TopKittiesPresenter {
    private var imgSession:IMGSession
    private var imgGalleryRequest: IMGGalleryRequest_Additions
    private weak var topKittiesView:TopKittiesView?
    
    private var lastPageLoaded = -1 //will always start from page 0
    private var lastPageAsked  = -1 //will always start from page 0
    lazy var queue: OperationQueue = {
        var queue = OperationQueue()
        queue.name = "Load more media queue"
        queue.maxConcurrentOperationCount = 1 //Not used now. Just in case we want to load more than 1 page at a time
        return queue
    }()
    
    
    init(withImgSession imgSession:IMGSession = IMGSession.sharedInstance(), withImgGalleryRequest imgGalleryRequest: IMGGalleryRequest_Additions = IMGGalleryRequest_Additions()) {
        self.imgSession = imgSession
        self.imgGalleryRequest = imgGalleryRequest
        reachability(startListening: true)
    }
    
    func attach(topKittiesView:TopKittiesView) {
        self.topKittiesView = topKittiesView
    }
    
    func deattachView() {
        reachability(startListening: false)
        topKittiesView = nil
    }
    
    func reachability(startListening: Bool){
        if startListening{
            NotificationCenter.default.addObserver(self, selector: #selector(didChangeInternetStatus(notification:)), name: .didChangeInternetStatus, object: nil)
        }else{
            NotificationCenter.default.removeObserver(self, name: .didChangeInternetStatus, object: nil)
        }
    }
    
    func getMoreCollectionViewData(forValue value: String) -> Promise<Void> {
        return Promise{ [weak self] seal in
            guard let `self` = self, lastPageAsked == self.lastPageLoaded else {return} //Only one page should be should be request at a time
            if !self.imgSession.reachability(){self.queue.isSuspended = true}
            self.topKittiesView?.startLoading(page: self.lastPageAsked)
            self.queue.addOperation { [weak self] in
                guard let `self` = self else{return}
                self.lastPageAsked += 1
                _ = self.imgGalleryRequest.searchGallery(searchFor: value, page: self.lastPageAsked, withIMGSession: self.imgSession)
                    .done{ [weak self] imgModels in
                        self?.lastPageLoaded += 1
                        self?.topKittiesView?.setData(withValues: imgModels)
                        seal.fulfill(())
                    }.ensure { [weak self] in
                        self?.topKittiesView?.stopLoading()
                    }.catch{ error in
                        seal.reject(error)
                }
            }
        }
    }
}

//MARJ: - PRivate Methods
extension TopKittiesPresenter{
    @objc private func didChangeInternetStatus(notification:Notification) {
        guard let userInfo = notification.userInfo, let status = userInfo["status"] as? AFNetworkReachabilityStatus else{return}
        switch status {
        case .notReachable:
            queue.isSuspended = true
            topKittiesView?.noInternet()
        default:
            queue.isSuspended = false
        }
    }
    
}
