//
//  CoverImageCell.swift.swift
//  Etquest-Wakoopa
//
//  Created by Vicky on 4/30/18.
//  Copyright © 2018 AleG. All rights reserved.
//

import UIKit
import ImgurSession
import SwiftGifOrigin

final class CoverImageCell: UICollectionViewCell {
    
    @IBOutlet weak var numberOfViews: UILabel!
    @IBOutlet weak var coverImage: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    private var cover:IMGImage?
    
    func loadCell(withImgModel imgModel: IMGModel) {
        coverImage.image = nil
        switch imgModel {
        case let galleryImage as IMGGalleryImage:
            cover = galleryImage.coverImage()
            set(title: galleryImage.title, numberOfViews: galleryImage.views, coverImage: cover)
        case let galleryAlbun as IMGBasicAlbum://IMGGalleryAlbum:
            cover = galleryAlbun.coverImage()
            set(title: galleryAlbun.title, numberOfViews: galleryAlbun.views, coverImage: cover)
        default:
            break
        }
    }
}

//MARK: - Private Methods
extension CoverImageCell{
    private func set(title: String?, numberOfViews: Int?, coverImage: IMGImage?){
        self.title.text = title
        self.numberOfViews.text = numberOfViews != nil ? String(numberOfViews!) : ""
        if let coverImage = coverImage{
            loadThumbnailImage(forImgImage: coverImage, imageSize: ImgSizeWithOriginal.mediumThumbnail)
        }
    }
    
    private func loadThumbnailImage(forImgImage image: IMGImage, imageSize size:ImgSizeWithOriginal){
        loadingIndicator.startAnimating()
        _ = IMGImageRequest.getImage(fromImgImage: image, withSize: size)
            .done{ [weak self] result in
                guard let cover = self?.cover,
                    let resultURL = URL(string: result.requestURL),
                    var askedURL = IMGImageRequest.getImageURL(imageImage: cover, size: size) else{return}
                
                if askedURL.lastPathComponent.contains(".gif"){
                    guard let giftAskedURL = IMGImageRequest.getImageURL(imageImage: cover, size: .original) else{return}
                    askedURL = giftAskedURL
                }
                guard resultURL.lastPathComponent == askedURL.lastPathComponent else{return}
                self?.coverImage.image = result.image
            }.ensure { [weak self] in
                self?.loadingIndicator.stopAnimating()
            }.catch{ [weak self] _ in
                guard let nextSize = IMGImageRequest.getNextSize(previousSize: size) else{return}
                self?.loadThumbnailImage(forImgImage: image, imageSize: nextSize)
        }
    }
}
