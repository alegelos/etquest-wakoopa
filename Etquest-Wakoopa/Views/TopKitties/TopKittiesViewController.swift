//
//  TopKittiesViewController.swift
//  Etquest-Wakoopa
//
//  Created by Vicky on 4/27/18.
//  Copyright © 2018 AleG. All rights reserved.
//

import UIKit
import ImgurSession
import PromiseKit

final class TopKittiesViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    private let searchKey = "cats"
    private var selectedMedia: IMGModel?
    
    private let topKittiesPresenter = TopKittiesPresenter()
    private let collectionSource = CollectionViewSource()
//    private let kSegueToMediaDetails = "GoToMediaDetails"

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.delegate = collectionSource
        collectionView.dataSource = collectionSource
        collectionSource.delegate = self
        
        
        topKittiesPresenter.attach(topKittiesView: self)
        _ = topKittiesPresenter.getMoreCollectionViewData(forValue: searchKey)
    }
}

//MARK: - CollectionViewSource delegate
extension TopKittiesViewController:CollectionViewSourceDelegate{
    func didSelect(item: IMGModel) {
        //TODO Show image in original size
    }
    
    func didScrollToAlmostBottom() {
        _ = topKittiesPresenter.getMoreCollectionViewData(forValue: searchKey)
    }
}

//MARK: - TopKittiesView delegate
extension TopKittiesViewController:TopKittiesView{
    func startLoading(page: Int) {
        guard page == 0 else{return}
        loadingIndicator.startAnimating()
    }
    
    func stopLoading() {
        loadingIndicator.stopAnimating()
    }
    
    func setData(withValues values:Array<IMGModel>){
        collectionSource.allMediaContent.append(contentsOf: values)
        collectionView.reloadData()
    }
    
    func showLoginView(){
        //TODO logout if fail refresh token
    }
    
    func noInternet() {
        if presentedViewController == nil {
            showNoInternetAlert()
        }
    }
}
