//
//  CollectionViewSource.swift
//  Etquest-Wakoopa
//
//  Created by Vicky on 4/30/18.
//  Copyright © 2018 AleG. All rights reserved.
//

import UIKit
import ImgurSession

protocol CollectionViewSourceDelegate: class {
    func didSelect(item: IMGModel)
    func didScrollToAlmostBottom()
}

final class CollectionViewSource: NSObject, UICollectionViewDataSource, UICollectionViewDelegate {
    let kCellIdentifier = "ImageCells"
    var allMediaContent:[IMGModel] = []
    weak var delegate: CollectionViewSourceDelegate?
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return allMediaContent.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kCellIdentifier, for: indexPath) as? CoverImageCell else{
            return UICollectionViewCell()
        }
        cell.loadCell(withImgModel: allMediaContent[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.didSelect(item: allMediaContent[indexPath.item])
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.item  == allMediaContent.count-41 || allMediaContent.count < 41 {//41 in order to start loading before it ends
            delegate?.didScrollToAlmostBottom()
        }
    }
}

////MARK: - ScrollView Delegate
//extension CollectionViewSource: UIScrollViewDelegate{
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        if isScrollContentCloseToMax(contentSizeHeight: scrollView.contentSize.height, contentOffSetY: scrollView.contentOffset.y){
//            delegate?.didScrollToAlmostBottom()
//        }
//    }
//}

////MARK: - Private Methods
//extension CollectionViewSource{
//    private func isScrollContentCloseToMax(contentSizeHeight: CGFloat, contentOffSetY: CGFloat) -> Bool{
//        return contentSizeHeight - contentOffSetY < 1000 && contentSizeHeight > 0
//    }
//}
