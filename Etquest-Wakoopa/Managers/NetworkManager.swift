//
//  NetworkManager.swift
//  Etquest-Wakoopa
//
//  Created by Vicky on 4/30/18.
//  Copyright © 2018 AleG. All rights reserved.
//

import Foundation
import PromiseKit
import SwiftGifOrigin

final class NetworkManager {
    
    static let shared = NetworkManager()
    private init(){}
    
    private let imageCache = NSCache<NSString, UIImage>()
    
    private var activeNetworkProcess = 0 {
        didSet{
            UIApplication.shared.isNetworkActivityIndicatorVisible = activeNetworkProcess > 0
        }
    }
    
    func getImage(fromUrl url: URL) -> Promise<(requestURL: String, image: UIImage)>{
        return Promise{ seal in
            if let cachedImage = self.imageCache.object(forKey: url.lastPathComponent as NSString) {
                seal.fulfill((url.absoluteString, cachedImage))
                return
            }
            
            self.updateNetworkActivityCounter(numberOfProcess: 1)
            _ = Alamofire.request(url).responseData()
                .done{ args -> () in
                    let (data, response) = args
                    guard let image = self.getImageFromResponse(data: data, rps: response) else{
                        seal.reject(NetworkError.failToGetImageFromData)
                        return
                    }
                    seal.fulfill(image)
                }.ensure { [unowned self] in
                    self.updateNetworkActivityCounter(numberOfProcess: -1)
                }.catch{ error in
                    seal.reject(error)
            }
        }
    }
}

//MARK: - Private Methods
extension NetworkManager{
    private func updateNetworkActivityCounter(numberOfProcess amount:Int){
        activeNetworkProcess = activeNetworkProcess + amount
    }
    
    private func getImageFromResponse(data: Data, rps: PMKAlamofireDataResponse) -> (requestURL: String, image: UIImage)?{
        var tempImage = UIImage(data: data)
        tempImage = tempImage != nil ? tempImage : UIImage.gif(data: data)
        guard let image = tempImage,
            let responseURL = rps.response?.url?.lastPathComponent else{
                return nil
        }
        self.imageCache.setObject(image, forKey: responseURL as NSString)
        return (responseURL, image)
    }
}
