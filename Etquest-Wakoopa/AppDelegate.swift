//
//  AppDelegate.swift
//  Etquest-Wakoopa
//
//  Created by Vicky on 4/26/18.
//  Copyright © 2018 AleG. All rights reserved.
//

import UIKit
import ImgurSession

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        guard let imgGurClientId = ImgGurPListUtils.get(dataForKey: .clientId) as? String,
              let imgGurSecretId = ImgGurPListUtils.get(dataForKey: .secretId) as? String else{return false}//TODO get missing data or do something without this service
        
        IMGSession.authenticatedSession(withClientID: imgGurClientId, secret: imgGurSecretId, authType: .tokenAuth, with: self)
        startReachabilityObserver()
        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        if let imgGurScheme = ImgGurPListUtils.get(dataForKey: .callBackScheme) as? String,
           let scheme = url.scheme,scheme == imgGurScheme,
           let fragments = url.getFragmentsKeyVals(), let refreshToken = fragments["refresh_token"]{
            IMGSession.sharedInstance().authenticate(withRefreshToken: refreshToken)
        }else{
            debugPrint(ImgGurError.failToGetRefreshToken)
        }
        return true
    }
}

//MARK: - ImgGur Delegate
extension AppDelegate:IMGSessionDelegate{
    func imgurSessionNeedsExternalWebview(_ url: URL!, completion: (() -> Void)!) {
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    func imgurRequestFailed(_ error: Error!) {
        debugPrint(error)
    }
    
    func imgurSessionNewNotifications(_ freshNotifications: [Any]!) {
        NotificationCenter.default.post(name: .didAuthenticateToImgGur, object: self)
    }
    
    func imgurSessionTokenRefreshed() {
        UserUtils.saveUser(withRefreshToken: IMGSession.sharedInstance().refreshToken)
    }
}

//MARK: - Private Methods
extension AppDelegate{
    func startReachabilityObserver() {
        AFNetworkReachabilityManager.shared().startMonitoring()
        
        AFNetworkReachabilityManager.shared().setReachabilityStatusChange({status in
            NotificationCenter.default.post(name: .didChangeInternetStatus, object: self, userInfo: ["status":status])
        })
    }
}

