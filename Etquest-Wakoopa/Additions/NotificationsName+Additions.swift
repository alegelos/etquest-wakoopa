//
//  NotificationsName+Additions.swift
//  Etquest-Wakoopa
//
//  Created by Vicky on 4/29/18.
//  Copyright © 2018 AleG. All rights reserved.
//

import Foundation

extension Notification.Name{
    static let didAuthenticateToImgGur = Notification.Name("didAuthenticateToImgGur")
    static let didChangeInternetStatus = Notification.Name("didChangeInternetStatus")
}
