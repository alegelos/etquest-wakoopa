//
//  URL+Additions.swift
//  Etquest-Wakoopa
//
//  Created by Vicky on 4/27/18.
//  Copyright © 2018 AleG. All rights reserved.
//

import Foundation

extension URL{
    func getFragmentsKeyVals() -> Dictionary<String, String>? {
        guard let keyValues = fragment?.components(separatedBy: "&"), keyValues.count > 0 else{
            return nil
        }
        var results = Dictionary<String,String>()

        for pair in keyValues {
            let kv = pair.components(separatedBy: "=")
            if kv.count > 1 {
                results[kv[0]] = kv[1]
            }
        }
        return results
    }
}
