//
//  ImgGur+Additions.swift
//  Etquest-Wakoopa
//
//  Created by Vicky on 4/28/18.
//  Copyright © 2018 AleG. All rights reserved.
//

import Foundation
import ImgurSession
import PromiseKit

//ImgGur library enums does not conform to String protocol. So..
enum GalleryWindow:String{
    case day, week, month, year, all
}

//ImgGur library enums does not conform to String protocol. So..
enum GallerySort:String{
    case time, viral, top
}

//ImgGur Library callbacks does not work if not auth. Will auth and not callback. Helping methods to continue the flow if not auth
//Use before doing a request to ensure correct flow
extension IMGSession{
    func reachability() -> Bool {
        guard let imgurReachability = imgurReachability else{return false}
        return imgurReachability.networkReachabilityStatus != .notReachable
    }
    
    func isSessionValid() -> Promise<Void> {
        return Promise{ seal in
            guard sessionAuthState() != .authenticated else{seal.reject(ImgGurError.notAuthenticated); return}
            seal.fulfill(())
        }
    }
}

enum ImgSizeWithOriginal {
    case smallSquare, bigSquare, smallThumbnail, mediumThumbnail, largeThumbnail, hugeThumbnail, original
}

extension IMGImageRequest{
    @discardableResult class func getImage(fromImgImage imgImage:IMGImage, withSize size:ImgSizeWithOriginal?) -> Promise<(requestURL: String, image: UIImage)>{
        return Promise{ seal in
            guard var imageURL = getImageURL(imageImage: imgImage, size: size) else{
                seal.reject(ImgGurError.failToGetImageUrl)
                return
            }
            
            if imageURL.lastPathComponent.contains(".gif"), let tempImageURL = getImageURL(imageImage: imgImage, size: .original){
                imageURL = tempImageURL
            }
            
            NetworkManager.shared.getImage(fromUrl: imageURL)
                .done{ result in
                    seal.fulfill(result)
                }.catch{ error in
                    seal.reject(error)
                }
        }
    }
    
    class func getImageURL(imageImage: IMGImage, size: ImgSizeWithOriginal?) -> URL?{
        guard let size = size else{return nil}
        var url:URL? = nil
        switch size {
        case .original:
            url = imageImage.url
        default:
            if let size = IMGSize(rawValue: size.hashValue){
                url = imageImage.url(with: size)
            }
        }
        return url
    }
    
    class func getNextSize(previousSize: ImgSizeWithOriginal?) -> ImgSizeWithOriginal?{
        guard let previousSize = previousSize else {return nil}
        
        let result:ImgSizeWithOriginal?
        switch previousSize {
        case .smallSquare:
            result = .bigSquare
        case .smallThumbnail:
            result = .mediumThumbnail
        case .mediumThumbnail:
            result = .largeThumbnail
        case .largeThumbnail:
            result = .hugeThumbnail
        case .hugeThumbnail, .bigSquare:
            result = .original
        default:
            result = nil
        }
        return result
    }
}

//ImgGur api is missing search for gallery. So..
class IMGGalleryRequest_Additions: IMGGalleryRequest{
    func searchGallery(searchFor searchText:String, page:Int = 0, sort: GallerySort = .time, forDate date: GalleryWindow = .all, withIMGSession imgSession: IMGSession = IMGSession.sharedInstance()) -> Promise<Array<IMGModel>>{
        return Promise{ seal in
            guard var path = IMGGalleryRequest.path() else{seal.reject(ImgGurError.failToGetBaseURL);return}
            
            path.append("/search")
            path.append("/" + sort.rawValue)
            path.append("/" + date.rawValue)
            path.append("/" + String(page))
            path.append("?q=" + searchText)
            
            imgSession.get(path, parameters: nil, success: {[weak self] task, responseObject -> () in
                guard let response = responseObject, let imgModels = self?.getIMGModels(fromResponse: response) else{return}
                seal.fulfill(imgModels)
            }, failure: {error -> () in
                guard let error = error else{seal.reject(ImgGurError.failToDoRequest);return}
                seal.reject(error)
            })
        }
    }
    
    private func getIMGModels(fromResponse response: Any) -> Array<IMGModel>?{
        guard let response = response as? [[String:Any]] else{return nil}
        
        var imgImages = Array<IMGModel>()
        for item in response {
            guard let isAlbun = item["is_album"] as? Bool, let model = getIMGModel(fromJSon: item, isAlbun: isAlbun) else{continue}
            imgImages.append(model)
        }
        return imgImages
    }
    
    private func getIMGModel(fromJSon json: Dictionary<String,Any>, isAlbun: Bool) -> IMGModel?{
        return isAlbun ? try? IMGGalleryAlbum(jsonObject: json) : try? IMGGalleryImage(jsonObject: json)
    }
}
