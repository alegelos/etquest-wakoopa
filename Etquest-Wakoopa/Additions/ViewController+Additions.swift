//
//  ViewController+Additions.swift
//  Etquest-Wakoopa
//
//  Created by Vicky on 4/29/18.
//  Copyright © 2018 AleG. All rights reserved.
//

import UIKit

extension UIViewController{
    func showNoInternetAlert() {
        let alert = UIAlertController(title: "Ups!", message: "No internet connection", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}
