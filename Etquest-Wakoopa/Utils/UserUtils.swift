//
//  UserUtils.swift
//  Etquest-Wakoopa
//
//  Created by Vicky on 4/29/18.
//  Copyright © 2018 AleG. All rights reserved.
//

import Foundation

struct UserUtils {
    static let kRefreshTokenKey = "kRefreshTokenKey"
    
    static func saveUser(withRefreshToken refreshToken: String) {
        UserDefaults.standard.set(refreshToken, forKey: kRefreshTokenKey)
    }
    
    static func getUserRefreshToken() -> String?{
        return UserDefaults.standard.object(forKey: kRefreshTokenKey) as? String
    }
}
