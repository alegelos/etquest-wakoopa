//
//  ImgGurPListUtils.swift
//  Etquest-Wakoopa
//
//  Created by Vicky on 4/27/18.
//  Copyright © 2018 AleG. All rights reserved.
//

import Foundation

struct ImgGurPListUtils {
    
    enum keys:String {
        case clientId, secretId, callBackScheme
    }
    
    static func get(dataForKey key: keys) -> Any?{
        guard let dic = PListUtils.getDic(fromPList: .ImgGur),
              let data = dic[key.rawValue] else{
            return nil
        }
        return data
    }
}
